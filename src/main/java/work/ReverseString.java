package work;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReverseString {
    private static final Logger logger = LogManager.getLogger(ReverseString.class);
    public static void main(String[] args) {
        String input = "���� � ����� ���� ����";
        logger.info(reverse(input));
    }
    public static String reverse(String s){
        char[] result = s.toCharArray();
        int indexInString = 0;
        int endWord;
        int endNotReverseString = s.length();
        while (indexInString < endNotReverseString){
            if (result[indexInString] != ' ') {
                indexInString++;
            } else {
                endWord = indexInString;
                indexInString = 0;
                moveWordToEnd(endWord, endNotReverseString, result);
                endNotReverseString -= endWord+1;
            }
        }
        return new String(result);
    }

    private static void moveWordToEnd(int lengthWord, int endNotReverseString, char[] chars){
        //���������� ����� �� ����� ����� � �����
        int positionReversedString = endNotReverseString;
        for (int i = lengthWord -1 ; i >= 0 ; i--) {
            char lastChar = chars[i];
            moveLettersLeftForOnePosition(i, positionReversedString, chars);
            chars[positionReversedString-1] = lastChar;
            positionReversedString--;
        }
        // ���������� ������
        moveLettersLeftForOnePosition(0, positionReversedString, chars);
        chars[positionReversedString-1] = ' ';
    }

    private static void moveLettersLeftForOnePosition(int beginLetter, int endLetter, char[] chars) {
        for (int j = beginLetter; j < endLetter - 1 ; j++) {
            chars[j] = chars[j+1];
        }
    }
}
